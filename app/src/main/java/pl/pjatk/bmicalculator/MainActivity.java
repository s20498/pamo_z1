package pl.pjatk.bmicalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.SeekBar; // for changing the tip percentage
import android.widget.SeekBar.OnSeekBarChangeListener; // SeekBar listener
import android.widget.TextView; // for displaying text

import java.text.NumberFormat; // for currency formatting

public class MainActivity extends AppCompatActivity {

//    // currency and percent formatter objects
    private static final NumberFormat bmiFormat = NumberFormat.getNumberInstance();

    private Double weight = 50.0;
    private TextView weightTextView;
    private Double height = 160.0;
    private TextView heightTextView;
    private Double bmi = 0.0;
    private TextView bmiTextView;

    // called when the activity is first created
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // call superclass onCreate
        setContentView(R.layout.activity_main); // inflate the GUI

        // get references to programmatically manipulated TextViews

        heightTextView = (TextView) findViewById(R.id.heightTextView);
        weightTextView = (TextView) findViewById(R.id.weightTextView);
        bmiTextView = (TextView) findViewById(R.id.bmiTextView);
        bmiTextView.setText(bmiFormat.format(0));

        // set weightSeekBar's OnSeekBarChangeListener
        SeekBar weightSeekBar =
                (SeekBar) findViewById(R.id.weightSeekBar);
        weightSeekBar.setOnSeekBarChangeListener(weightSeekBarListener);

        SeekBar heightSeekBar =
                (SeekBar) findViewById(R.id.heightSeekBar);
        heightSeekBar.setOnSeekBarChangeListener(heightSeekBarListener);
    }

    // calculate and display tip and total amounts
    private void calculate() {
        double bmi = weight / Math.pow(height, 2);
        bmiTextView.setText(bmiFormat.format(bmi));
    }

    // listener object for the SeekBar's progress changed events
    private final OnSeekBarChangeListener weightSeekBarListener =
            new OnSeekBarChangeListener() {
                // update percent, then call calculate
                @Override
                public void onProgressChanged(SeekBar weightSeekBar, int progress,
                                              boolean fromUser) {
                    weight = (double) weightSeekBar.getProgress();
                    weightTextView.setText(String.valueOf(weight) + "kg");
                    calculate(); // calculate and display tip and total
                }

                @Override
                public void onStartTrackingTouch(SeekBar weightSeekBar) { }

                @Override
                public void onStopTrackingTouch(SeekBar weightSeekBar) { }
            };

    private final OnSeekBarChangeListener heightSeekBarListener =
            new OnSeekBarChangeListener() {
                // update percent, then call calculate
                @Override
                public void onProgressChanged(SeekBar heightSeekBar, int progress,
                                              boolean fromUser) {
                    height = (double) (heightSeekBar.getProgress() / 100.0);
                    heightTextView.setText(String.valueOf(height) + "m");
                    calculate(); // calculate and display tip and total
                }

                @Override
                public void onStartTrackingTouch(SeekBar heightSeekBar) { }

                @Override
                public void onStopTrackingTouch(SeekBar heightSeekBar) { }
            };

//    // listener object for the EditText's text-changed events
//    private final TextWatcher bmiEditTextWatcher = new TextWatcher() {
//        // called when the user modifies the bill amount
//        @Override
//        public void onTextChanged(CharSequence s, int start,
//                                  int before, int count) {
//
//            try { // get bill amount and display currency formatted value
//                bmi = Double.parseDouble(s.toString()) / 100.0;
//                bmiTextView.setText(bmiFormat.format(bmi));
//            }
//            catch (NumberFormatException e) { // if s is empty or non-numeric
//                bmiTextView.setText("");
//            }
//
//            calculate(); // update the tip and total TextViews
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) { }
//
//        @Override
//        public void beforeTextChanged(
//                CharSequence s, int start, int count, int after) { }
//    };
}


/*************************************************************************
 * (C) Copyright 1992-2016 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
